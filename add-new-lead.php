
<?php

// include('header.php');


?>

<!-- <input type="button" onclick="addInput()" value="Add More Notes"/> -->

<span id="responce"></span>

<div class="container">
 
  <!-- Trigger the modal with a button -->
  <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#mylead">Add Lead</button> -->

  <!-- Modal -->
  <div class="modal fade" id="mylead" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Lead</h4>
                </div>
                <div class="modal-body">
					<div class="form-group" id="hide_duplicate" style="display: none">
						 
						  <div class="col-sm-12">
							<h3 class="allredy" style="text-align:center;">Already Exist</h3>
							
						  </div>
					</div>
					  
					  <form class="form-horizontal" method="post" id="addlead">
						<div class="form-group">
						  <label class="control-label col-sm-12" for="name">Name:</label>
						  <div class="col-sm-12">
							<input type="text" class="form-control" id="cname" placeholder="Customer Name" name="cname">
						  </div>
						</div>
						<div class="form-group">
						  <label class="control-label col-sm-12" for="email">Email:</label>
						  <div class="col-sm-12">
							<input type="email" class="form-control" id="email" placeholder="Enter Email" name="cemail">
						  </div>
						</div>
						<div class="form-group">
						  <label class="control-label col-sm-12" for="email">Skype Id:</label>
						  <div class="col-sm-12">
							<input type="text" class="form-control" id="skype" placeholder="Enter Skype Id" name="skype">
						  </div>
						</div>
                       <div class="form-group">
						  <label class="control-label col-sm-12" for="mobile">Mobile:</label>
						  <div class="col-sm-12">
							<input type="text" class="form-control" id="cmobile" placeholder="Enter Mobile Number" name="cmobile">
						  </div>
						</div>
                        <div class="form-group">
						  <label class="control-label col-sm-12" for="technology">Technology:</label>
						  <div class="col-sm-12">
						  	<select class="form-control" id="technology" placeholder="Technology" name="technology">
                                        <option value="sel">Please Select</option>
										<option value="Wordpress">Wordpress</option>
										<option value="Shopify">Shopify</option>
										<option value="Magento">Magento</option>
										<option value="Codeigniter">Codeigniter</option>
										<option value="Laravel">Laravel</option>
							</select>
							<!-- <input type="text" class="form-control" id="technology" placeholder="Technology" name="technology"> -->
						  </div>
						</div>
						<div class="form-group">
						  <label class="control-label col-sm-12" for="Project Title">Project Title:</label>
						  <div class="col-sm-12">
							<input type="text" class="form-control" id="project_title" placeholder="Project Title" name="project_title">
						  </div>
						</div>
						<div class="form-group">
						  <label class="control-label col-sm-12" for="description">Description:</label>
						  <div class="col-sm-12">
                          <textarea class="form-control" id="project_desc" placeholder="Enter Description" name="project_desc"></textarea>
							
						  </div>
						</div>
					<!-- <div class="form-group">
						  <label class="control-label col-sm-12" for="generated">Who Generated:</label>
						  <div class="col-sm-12">
							<input type="text" class="form-control" id="generated" placeholder="Who Generated" name="generated">
						  </div>
						</div> -->
						<div class="form-group">
						  <label class="control-label col-sm-12" for="type">Status:</label>
							<div class="col-sm-12">
									   <select class="form-control" id="project_type" name="project_type">
                                        <option value="0">Please Select</option>
										<option value="hot">Hot</option>
										<option value="cold">Cold</option>
										<option value="converted">Converted</option>
									  </select>
                          </div>
						</div>
						<!-- <div class="form-group">
						  <label class="control-label col-sm-2" for="email">Notes:</label>
						  <div class="col-sm-10">
							<textarea class="form-control" id="note" placeholder="Notes" name="notes"></textarea>
						  </div>
						</div> -->
						<div class="form-group">        
						  <div class="col-sm-12">
							<input type="submit" name="submit" class="btn btn-default" id="submit" value="Submit">
							
						  </div>
						</div>
					  </form>
				
		</div>
               
            </div>
        </div>
    </div>
  
</div>

<script type="text/javascript">

$(document).ready(function() {


      $('#submit').click(function(e){
        e.preventDefault();


        var name = $("#cname").val();
        var email = $("#email").val();
       	var mobile = $("#cmobile").val();
       	var skype = $("#skype").val();
	    var tech = $("#technology").val();
	    var project_title = $("#project_title").val();
	    var project_desc = $("#project_desc").val();
	   // var gen = $("#generated").val();
	    var pro_type = $("#project_type").val();
	   // var note = $("#note").val();
        
        var dataString = 'cname=' + name  + '&email=' + email + '&cmobile=' + mobile +'&skype=' + skype + '&technology=' + tech + '&project_title=' + project_title + '&project_desc=' + project_desc  + '&project_type=' + pro_type ;
       
       //console.log(dataString);
    	if (name == '' || email == '' || mobile == '' || tech == '' || project_title == '' || project_desc == '' ||  pro_type == '') {
			alert("Please Fill All Fields");
			} else {
			// AJAX code to submit form.
					$.ajax({
							type: "POST",
							url: "http://localhost/lead/sub.php",
							data: dataString,
							cache: false,
							success: function(data) {
								//alert(data);
									console.log(data);
									if(data==1)
									{
										
		                                $("#hide_duplicate").css("display","block"); 
									}
									if(data==2){
										
										//$("#addlead").find("input[type=text],input[type=email],input[type=tel], textarea").val("");
										//$("#project_type").val('0');
										//$("#technology").val('sel');
										$("#hide_duplicate").css("display","none"); 
										$('#mylead').modal('hide');
										window.location.reload();
									}
									
								}
											
								
					});
			}
			return false;


      });
  });



</script>







     
 